//Objective: Create a server-side app using Express Web Framework
/* Append the entire app to our node package manager (package.json)
	npm init or npm init -y (recommended)
	npm init will ask you many inputs but in the end you can just change it manually in your package.json file
*/
/*Tips: relate tasks to something that you do on real life 
	1. Identify the requirements that needed to prepare
		npm install express nodemon or you can install it individually

		-express (npm install express) -> main component to create the server
			->require() - To be able to gather the utilities and components needed that the express library will provide us
			 Ex. const variableName = require("express");
		-nodemon -> to Create a Runtime environment that automatically autofix all the changes in our app to save time by installing nodemon inside your directory folder (No need to restart server if there is changes)
			Inside Terminal -> npm install nodemon 
				then nodemon <filename> Ex. nodemon index.js
				If batch not found then type npm install -g nodemon
				Then try nodemon <filename> again
				or you can put it in your package.json script

				Ctrl + c to cancel to process
		
		Create Remote repository for our Project -> For backup preparation
		NOTE: always DISABLE the node_modules folder because it will too much space in our repository and if ever that you will deploy your node project on deployment platforms (heroku, netlify, vercel) the project will automatically be rejected because node_modules is not recognize

		To disable the node_module folder -> git init -> touch .gitignore 
		-> inside your .gitignore file, put <foldername>/node_modules 
												Ex. d1/node_modules


	2. 
*/

const express = require("express"); //"express" is in the package.json script

console.log(`
		Welcome to our Express API Server
			_░▒███████
			░██▓▒░░▒▓██
			██▓▒░__░▒▓██___██████
			██▓▒░____░▓███▓__░▒▓██
			██▓▒░___░▓██▓_____░▒▓██
			██▓▒░_______________░▒▓██
			_██▓▒░______________░▒▓██
			__██▓▒░____________░▒▓██
			___██▓▒░__________░▒▓██
			____██▓▒░________░▒▓██
			_____██▓▒░_____░▒▓██
			______██▓▒░__░▒▓██
			_______█▓▒░░▒▓██
			_________░▒▓██
			_______░▒▓██
			_____░▒▓██
	`)

